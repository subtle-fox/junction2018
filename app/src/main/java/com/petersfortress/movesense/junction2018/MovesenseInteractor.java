package com.petersfortress.movesense.junction2018;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.movesense.mds.Mds;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsResponseListener;
import com.petersfortress.movesense.junction2018.models.Point;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import rx.subjects.PublishSubject;

import static com.petersfortress.movesense.junction2018.CommonsHelper.DELAY_UPDATE;
import static com.petersfortress.movesense.junction2018.CommonsHelper.NO_MOTION_RANGE;
import static com.petersfortress.movesense.junction2018.CommonsHelper.NUMBER_OF_NO_MOTION_TO_SOS;
import static com.petersfortress.movesense.junction2018.CommonsHelper.NUMBER_OF_SHAKES_TO_SOS;
import static com.petersfortress.movesense.junction2018.CommonsHelper.QUEUE_SIZE;
import static com.petersfortress.movesense.junction2018.CommonsHelper.SHAKE_LAMP_TIME;
import static com.petersfortress.movesense.junction2018.CommonsHelper.SOS_RANGE;
import static com.petersfortress.movesense.junction2018.FormatHelper.SCHEME_PREFIX;

public class MovesenseInteractor implements DataFlowInteractor {

    private static final String LOG_TAG = MovesenseInteractor.class.getName();

    private String serialNumber;

    private long previousShakeTime = -1L;
    private long previousFallTime = -1L;

    private boolean ledRequested = false;
    private boolean sosModeIsOn = false;

    private final String LED_PATH = "/Component/Led";
    private final String LED_PARAMETER = "{\"isOn\":";

    private Queue<Double> xQueue = new ArrayBlockingQueue<>(QUEUE_SIZE);
    private Queue<Double> yQueue = new ArrayBlockingQueue<>(QUEUE_SIZE);
    private Queue<Double> zQueue = new ArrayBlockingQueue<>(QUEUE_SIZE);

    private Handler uiHandler;
    private Context context;
    private SmsProvider smsProvider;
    private PublishSubject<String> alertObservable = PublishSubject.create();

    public MovesenseInteractor(Context context, String serialNumber, Looper looper) {
        this.context = context;
        this.serialNumber = serialNumber;
        this.uiHandler = new Handler(looper);
        this.smsProvider = new SmsProvider();

        alertObservable
                .throttleFirst(30, TimeUnit.SECONDS)
                .doOnNext(message -> Log.d(LOG_TAG, "sending sms: " + message))
                .subscribe(message -> smsProvider.sendSms(message));
    }

    @Override
    public void onUpdated(Point point) {
        collectData(point.x, point.y, point.z);

        if (isShakeDetected(xQueue.toArray(), yQueue.toArray(), zQueue.toArray())) {
            long now = SystemClock.elapsedRealtime();
            if (now - previousShakeTime > DELAY_UPDATE) {
                previousShakeTime = now;
                Toast.makeText(context, "shake detected", Toast.LENGTH_SHORT).show();
                smsProvider.sendSms("Rescue requested! Hurry up!");
                turnLightForTime(SHAKE_LAMP_TIME);
            }
        }

        if (isFallDetected(xQueue.toArray(), yQueue.toArray(), zQueue.toArray())) {
            long now = SystemClock.elapsedRealtime();
            if (now - previousFallTime > 11000) {
                previousFallTime = now;
                Toast.makeText(context, "fall detected!", Toast.LENGTH_SHORT).show();
                sendSms("Patient is unconscious, he needs your help ASAP!");
                showSosLight();
            }
        }
    }

    private void sendSms(String message) {
        alertObservable.onNext(message);
    }

    private synchronized void collectData(double x, double y, double z) {
        if (xQueue.size() == QUEUE_SIZE) xQueue.poll();
        if (yQueue.size() == QUEUE_SIZE) yQueue.poll();
        if (zQueue.size() == QUEUE_SIZE) zQueue.poll();

        xQueue.add(x);
        yQueue.add(y);
        zQueue.add(z);
    }

    private boolean isShakeDetected(Object[] xArray, Object[] yArray, Object[] zArray) {
        int fullAmplitudeCounter = 0;

        for (int i = 0; i < xArray.length - 1; i++) {
            Double previous = (Double) xArray[i];
            Double next = (Double) xArray[i + 1];
            if (Math.abs(previous - next) >= SOS_RANGE) {
                fullAmplitudeCounter++;
            }
        }
        for (int i = 0; i < yArray.length - 1; i++) {
            Double previous = (Double) yArray[i];
            Double next = (Double) yArray[i + 1];
            if (Math.abs(previous - next) >= SOS_RANGE) {
                fullAmplitudeCounter++;
            }
        }
        for (int i = 0; i < zArray.length - 1; i++) {
            Double previous = (Double) zArray[i];
            Double next = (Double) zArray[i + 1];
            if (Math.abs(previous - next) >= SOS_RANGE) {
                fullAmplitudeCounter++;
            }
        }

        return fullAmplitudeCounter > NUMBER_OF_SHAKES_TO_SOS;
    }

    private boolean isFallDetected(Object[] xArray, Object[] yArray, Object[] zArray) {
        double noChangeCounter = 0;

        for (int i = 0; i < xArray.length - 1; i++) {
            Double previous = (Double) xArray[i];
            Double next = (Double) xArray[i + 1];
            if (previous - next <= NO_MOTION_RANGE) {
                noChangeCounter++;
            }
        }
        for (int i = 0; i < yArray.length - 1; i++) {
            Double previous = (Double) yArray[i];
            Double next = (Double) yArray[i + 1];
            if (previous - next <= NO_MOTION_RANGE) {
                noChangeCounter++;
            }
        }
        for (int i = 0; i < zArray.length - 1; i++) {
            Double previous = (Double) zArray[i];
            Double next = (Double) zArray[i + 1];
            if (previous - next <= NO_MOTION_RANGE) {
                noChangeCounter++;
            }
        }

        return noChangeCounter > NUMBER_OF_NO_MOTION_TO_SOS;
    }


    private synchronized void changeLedState(boolean setOn) {
        if (!ledRequested) {
            ledRequested = true;

            Mds.builder()
                    .build(context)
                    .put(SCHEME_PREFIX + serialNumber + LED_PATH,
                            LED_PARAMETER + setOn + "}",
                            new MdsResponseListener() {
                                @Override
                                public void onSuccess(String data) {
                                    Log.d(LOG_TAG, "onSuccess: " + data);
                                    ledRequested = false;
                                }

                                @Override
                                public void onError(MdsException error) {
                                    Log.e(LOG_TAG, "onError()", error);
                                    ledRequested = false;
                                }
                            });
        }
    }

    private void turnLightForTime(long millis) {
        changeLedState(true);
        uiHandler.postDelayed(() -> changeLedState(false), millis);
    }

    private synchronized void showSosLight() {
        sosModeIsOn = true;

        int initTime = 0;
        int pauseTime = 300;
        int firstLetterTime = 200;
        int firstLetterDelay = initTime + pauseTime;

        uiHandler.postDelayed(() -> changeLedState(true), firstLetterDelay);
        uiHandler.postDelayed(() -> changeLedState(false), firstLetterDelay + firstLetterTime);
        uiHandler.postDelayed(() -> changeLedState(true), firstLetterDelay + firstLetterTime * 2);
        uiHandler.postDelayed(() -> changeLedState(false), firstLetterDelay + firstLetterTime * 3);
        uiHandler.postDelayed(() -> changeLedState(true), firstLetterDelay + firstLetterTime * 4);
        uiHandler.postDelayed(() -> changeLedState(false), firstLetterDelay + firstLetterTime * 5);

        int secondLetterDelay = firstLetterDelay + 6 * firstLetterTime + pauseTime;
        int secondLetterTime = 600;

        uiHandler.postDelayed(() -> changeLedState(true), secondLetterDelay);
        uiHandler.postDelayed(() -> changeLedState(false), secondLetterDelay + secondLetterTime);
        uiHandler.postDelayed(() -> changeLedState(true), secondLetterDelay + secondLetterTime * 2);
        uiHandler.postDelayed(() -> changeLedState(false), secondLetterDelay + secondLetterTime * 3);
        uiHandler.postDelayed(() -> changeLedState(true), secondLetterDelay + secondLetterTime * 4);
        uiHandler.postDelayed(() -> changeLedState(false), secondLetterDelay + secondLetterTime * 5);

        int thirdLetterDelay = secondLetterDelay + 6 * secondLetterTime + pauseTime;
        int thirdLetterTime = 200;

        uiHandler.postDelayed(() -> changeLedState(true), thirdLetterDelay);
        uiHandler.postDelayed(() -> changeLedState(false), thirdLetterDelay + thirdLetterTime);
        uiHandler.postDelayed(() -> changeLedState(true), thirdLetterDelay + thirdLetterTime * 2);
        uiHandler.postDelayed(() -> changeLedState(false), thirdLetterDelay + thirdLetterTime * 3);
        uiHandler.postDelayed(() -> changeLedState(true), thirdLetterDelay + thirdLetterTime * 4);
        uiHandler.postDelayed(() -> changeLedState(false), thirdLetterDelay + thirdLetterTime * 5);
        uiHandler.postDelayed(() -> sosModeIsOn = false, thirdLetterDelay + thirdLetterTime * 5);
    }
}
