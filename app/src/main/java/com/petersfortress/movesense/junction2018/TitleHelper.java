package com.petersfortress.movesense.junction2018;

public class TitleHelper {

    private static String BRAND_NAME = "Movesense";
    private static int SHORT_NUMBER_INDEX = 18;

    public static String createTitleFromName(String name) {
        // original: Movesense 175030000366
        String shortNumber = name.substring(18);
        return BRAND_NAME + " *" + shortNumber;
    }
}
