package com.petersfortress.movesense.junction2018;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class SmsProvider {
    private final String basicUrl = "https://api.46elks.com/";
    private final String destPhone = "+79675239573";
    private final String from = "Junction18";

    private Response intercept(Interceptor.Chain chain) throws IOException {
        String credentials = Credentials.basic("u243b9d1f368d7f42a0138ea73ea12e89",
                "C72FD25C84880C130FC9D37591F71E46");

        Request request = chain.request();
        Request authenticatedRequest = request.newBuilder()
                .header("Authorization", credentials).build();
        return chain.proceed(authenticatedRequest);
    }

    private OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(this::intercept);
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        return builder.build();
    }

    private Gson provideGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    private Retrofit provideRetrofit(String baseUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }

    private RescueApi provideApi() {
        OkHttpClient okhttp = provideOkHttpClient();
        Retrofit retrofit = provideRetrofit(basicUrl, okhttp);
        return retrofit.create(RescueApi.class);
    }

    public void sendSms(String message) {
        Call<ResponseBody> bodyCall = provideApi().sendSms(from, destPhone, message);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
