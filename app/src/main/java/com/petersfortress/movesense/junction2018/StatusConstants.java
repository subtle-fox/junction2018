package com.petersfortress.movesense.junction2018;

public class StatusConstants {
    public static String STATUS_TITLE = "Status: ";
    public static String SCANNING = STATUS_TITLE + "Scanning...";
    public static String READY_TO_SCAN = STATUS_TITLE + "Ready to scan";
    public static String DETECTED = STATUS_TITLE + "Sensor(s) detected";
    public static String CONNECTED = STATUS_TITLE + "Connected";
}
