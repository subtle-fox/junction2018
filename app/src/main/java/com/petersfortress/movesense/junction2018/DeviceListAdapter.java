package com.petersfortress.movesense.junction2018;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import static com.petersfortress.movesense.junction2018.TitleHelper.createTitleFromName;

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.MyViewHolder> {
    private ArrayList<MyScanResult> mDataset = new ArrayList<>();
    private DeviceListInterface callback;
    private Context context;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView = itemView.findViewById(R.id.name);

        public MyViewHolder(View itemView) {
            super(itemView);
        }
    }

    public DeviceListAdapter(DeviceListInterface callback, Context context) {
        this.callback = callback;
        this.context = context;
    }

    public void setDataset(ArrayList<MyScanResult> mDataset) {
        this.mDataset = mDataset;
    }

    @Override
    public DeviceListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.device_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            callback.onClick(position);
        });

        holder.itemView.setOnLongClickListener(v -> {
            callback.onLongClick(position);
            return false;
        });

        holder.mTextView.setText(createTitleFromName(mDataset.get(position).name));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}