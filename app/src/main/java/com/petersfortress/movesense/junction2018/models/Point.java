package com.petersfortress.movesense.junction2018.models;

import java.util.Locale;

public class Point {
    public final double x, y, z;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "{%.2f, %.2f, %.2f}", x, y, z);
    }
}
