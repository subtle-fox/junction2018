package com.petersfortress.movesense.junction2018;

public class CommonsHelper {
    public final static double NO_MOTION_RANGE = 0.05;
    public final static long DELAY_UPDATE = 1000;
    public final static long SHAKE_LAMP_TIME = 5000;
    public final static long SOS_RANGE = 20;
    public final static int NUMBER_OF_SHAKES_TO_SOS = 4;
    public final static int NUMBER_OF_NO_MOTION_TO_SOS = 25;
    public final static int QUEUE_SIZE = 10;

}
