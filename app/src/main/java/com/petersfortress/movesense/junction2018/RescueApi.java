package com.petersfortress.movesense.junction2018;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RescueApi {
    @FormUrlEncoded
    @POST("a1/SMS")
    Call<ResponseBody> sendSms(@Field("from") String from, @Field("to") String to, @Field("message") String message);
}