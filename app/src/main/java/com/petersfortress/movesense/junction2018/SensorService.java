package com.petersfortress.movesense.junction2018;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.movesense.mds.Mds;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsSubscription;
import com.petersfortress.movesense.junction2018.models.LinearAcceleration;
import com.petersfortress.movesense.junction2018.models.Point;

public class SensorService extends Service {
    private final String LINEAR_ACCELERATION_PATH = "Meas/Acc/13";
    public static final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    private String LOG_TAG = SensorService.class.getName();
    private final static int NOTIFICAtION_ID = 100;

    public final static String MOVESENSE_SERIAL = "movesense.serial_number";
    public final static String ACTION_CONNECT = "action.connect";
    public final static String ACTION_DISCONNECT = "action.disconnect";
    public final static String ACTION_TRACK = "action.track";
    public final static String ACTION_UNTRACK = "action.untrack";

    private Gson gson = new Gson();


    // MDS singleton
    static Mds mMds;

    // TODO
    DataFlowInteractor interactor = null;
    private String connectedSerial;
    private MdsSubscription mdsSubscription;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        connectedSerial = intent.getStringExtra(MOVESENSE_SERIAL);
        interactor = new MovesenseInteractor(this, connectedSerial, getMainLooper());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("channel", "general notfication", NotificationManager.IMPORTANCE_DEFAULT);

            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(false)
                .setContentTitle("Movesense");

        switch (intent.getAction()) {
            case ACTION_CONNECT:
                Log.d(LOG_TAG, "connecting to device id: " + connectedSerial);
                builder.setContentText("Movesense is connecting...");
                break;
            case ACTION_DISCONNECT:
                Log.d(LOG_TAG, "connecting to device id: " + connectedSerial);
                untrack();
                disconnect(connectedSerial);
                stopSelf();
                return START_NOT_STICKY;
            case ACTION_TRACK:
                Log.d(LOG_TAG, "start tracking device: " + connectedSerial);
                builder.setContentText("Movesense is tracking...");
                builder.addAction(new NotificationCompat.Action(0, "stop", getStopTrackingIntent(connectedSerial)));
                track();
                break;
            case ACTION_UNTRACK:
                Log.d(LOG_TAG, "stop tracking device: " + connectedSerial);
                untrack();
                builder.addAction(new NotificationCompat.Action(0, "start", getStartTrackingIntent(connectedSerial)));
                break;
        }

        builder.setContentIntent(getContentIntent());
        builder.addAction(0, "disconnect", getDisconnectIntent(connectedSerial));
        startForeground(NOTIFICAtION_ID, builder.build());

        return super.onStartCommand(intent, flags, startId);
    }

    PendingIntent getContentIntent() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        return PendingIntent.getActivity(this, 0, mainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    PendingIntent getDisconnectIntent(String serial) {
        Intent cancelIntent = new Intent(this, SensorService.class);
        cancelIntent.setAction(ACTION_DISCONNECT).putExtra(MOVESENSE_SERIAL, serial);
        return PendingIntent.getService(this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    PendingIntent getStopTrackingIntent(String serial) {
        Intent cancelIntent = new Intent(this, SensorService.class);
        cancelIntent.setAction(ACTION_UNTRACK).putExtra(MOVESENSE_SERIAL, serial);
        return PendingIntent.getService(this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    PendingIntent getStartTrackingIntent(String serial) {
        Intent cancelIntent = new Intent(this, SensorService.class);
        cancelIntent.setAction(ACTION_TRACK).putExtra(MOVESENSE_SERIAL, serial);
        return PendingIntent.getService(this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void connect() {

    }

    void disconnect(String deviceAddress) {
        MainActivity.mMds.disconnect(deviceAddress);

    }

    void untrack() {
        mdsSubscription.unsubscribe();
    }

    void track() {
        mdsSubscription = Mds.builder().build(this).subscribe(URI_EVENTLISTENER,
                FormatHelper.formatContractToJson(connectedSerial, LINEAR_ACCELERATION_PATH),
                new MdsNotificationListener() {
                    @Override
                    public void onNotification(String data) {
                        Log.d(LOG_TAG, "onSuccess(): " + data);
                        LinearAcceleration linearAccelerationData = gson.fromJson(data, LinearAcceleration.class);
                        if (linearAccelerationData != null) {
                            LinearAcceleration.Array arrayData = linearAccelerationData.body.array[0];
                            double x = arrayData.x;
                            double y = arrayData.y;
                            double z = arrayData.z;
                            interactor.onUpdated(new Point(x, y, z));
                        }
                    }

                    @Override
                    public void onError(MdsException error) {
                        Log.e(LOG_TAG, "onError(): ", error);
                        Toast.makeText(SensorService.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}