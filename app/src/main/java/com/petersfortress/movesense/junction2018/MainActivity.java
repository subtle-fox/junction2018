package com.petersfortress.movesense.junction2018;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.movesense.mds.Mds;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.scan.ScanResult;
import com.polidea.rxandroidble.scan.ScanSettings;

import java.util.ArrayList;

import rx.Subscription;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;
import static com.petersfortress.movesense.junction2018.SensorService.ACTION_CONNECT;
import static com.petersfortress.movesense.junction2018.SensorService.ACTION_TRACK;
import static com.petersfortress.movesense.junction2018.SensorService.MOVESENSE_SERIAL;

public class MainActivity extends AppCompatActivity implements DeviceListInterface{
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    // MDS singleton
    static Mds mMds;

    // BleClient singleton
    static private RxBleClient mBleClient;

    // UI
    private TextView statusText;
    private RecyclerView deviceListView;
    private LinearLayoutManager layoutManager;

    private static ArrayList<MyScanResult> mScanResArrayList = new ArrayList<>();
    DeviceListAdapter mScanResArrayAdapter;

    Subscription mScanSubscription;

    static final String KEY_DEVCICE = "movesense_device";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnScan = findViewById(R.id.buttonScan);
        btnStop = findViewById(R.id.buttonScanStop);
        progress = findViewById(R.id.progress);
        statusText = findViewById(R.id.statusLabel);

        // Init Scan UI
        deviceListView = findViewById(R.id.listScanResult);
        layoutManager = new LinearLayoutManager(this);
        deviceListView.setLayoutManager(layoutManager);
        mScanResArrayAdapter = new DeviceListAdapter(this, this);
        deviceListView.setAdapter(mScanResArrayAdapter);
        statusText.setText(StatusConstants.READY_TO_SCAN);

        // Make sure we have all the permissions this app needs
        if (hasPermissions()) {
            requestPermissions();
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String savedDevice = sharedPreferences.getString(KEY_DEVCICE, null);
        if (savedDevice != null) {
            connectBLEDevice(savedDevice);
        }

        // Initialize Movesense MDS library
        initMds();
    }

    private RxBleClient getBleClient() {
        // Init RxAndroidBle (Ble helper library) if not yet initialized
        if (mBleClient == null) {
            mBleClient = RxBleClient.create(this);
        }

        return mBleClient;
    }

    private void initMds() {
        if (mMds == null) {
            mMds = Mds.builder().build(this);
        }
    }

    void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
    }

    boolean hasPermissions() {
        return ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED;
    }

    View btnScan;
    View btnStop;
    View progress;

    public void onScanClicked(View view) {
        setStatus(StatusConstants.SCANNING);
        progress.setVisibility(View.VISIBLE);

        btnScan.setVisibility(View.INVISIBLE);
        btnStop.setVisibility(View.VISIBLE);

        // Start with empty list
        mScanResArrayList.clear();
        mScanResArrayAdapter.notifyDataSetChanged();

        mScanSubscription = getBleClient()
                .scanBleDevices(new ScanSettings.Builder().build())
                .doOnNext(result -> Log.d(LOG_TAG, "scanResult: " + result))
                .filter(this::isMovesense)
                .doOnNext(result -> Log.d(LOG_TAG, "filtered: " + result))
                .doOnError(t -> Log.e(LOG_TAG, "scan error: " + t))
                .subscribe(this::onMovesenseDeviceScanned, throwable -> stopScan());
    }

    public boolean isAlreadyStored(ScanResult result) {
        String storedDevice = PreferenceManager.getDefaultSharedPreferences(this).getString(KEY_DEVCICE, null);
        if (storedDevice == null) {
            return false;
        } else {
            Log.d(LOG_TAG, "found stored device => connect automatically");
            return storedDevice.equals(result.getBleDevice().getMacAddress());
        }
    }

    public boolean isMovesense(ScanResult result) {
        return result.getBleDevice() != null
                && result.getBleDevice().getName() != null
                && result.getBleDevice().getName().startsWith("Movesense");
    }

    public void onMovesenseDeviceScanned(final ScanResult scanResult) {
        setStatus(StatusConstants.DETECTED);
        MyScanResult msr = new MyScanResult(scanResult);
        if (mScanResArrayList.contains(msr)) {
            mScanResArrayList.set(mScanResArrayList.indexOf(msr), msr);
        } else {
            mScanResArrayList.add(0, msr);
        }
        mScanResArrayAdapter.setDataset(mScanResArrayList);
        mScanResArrayAdapter.notifyDataSetChanged();
    }

    public void onStopScanClicked(View view) {
        stopScan();
    }

    private void stopScan() {
        progress.setVisibility(View.INVISIBLE);
        if (mScanSubscription != null) {
            mScanSubscription.unsubscribe();
            mScanSubscription = null;
        }

        btnScan.setVisibility(View.VISIBLE);
        btnStop.setVisibility(View.GONE);

        if (statusText.getText() == StatusConstants.SCANNING) {
            statusText.setText(StatusConstants.READY_TO_SCAN);
        }
    }

    @Override
    public void onClick(int position) {
        if (position < 0 || position >= mScanResArrayList.size())
            return;


        MyScanResult device = mScanResArrayList.get(position);
        if (!device.isConnected()) {
            // Stop scanning
            stopScan();

            // And connect to the device
            connectBLEDevice(device.macAddress);
        }
    }

    @Override
    public void onLongClick(int position) {
        if (position < 0 || position >= mScanResArrayList.size())
            return;

        MyScanResult device = mScanResArrayList.get(position);

        Log.i(LOG_TAG, "Disconnecting from BLE device: " + device.macAddress);
        mMds.disconnect(device.macAddress);
    }

    private void connectBLEDevice(String macAddress) {
        progress.setVisibility(View.VISIBLE);

        RxBleDevice bleDevice = getBleClient().getBleDevice(macAddress);
        final Activity me = this;
        Log.i(LOG_TAG, "Connecting to BLE device: " + bleDevice.getMacAddress());
        mMds.connect(bleDevice.getMacAddress(), new MdsConnectionListener() {

            @Override
            public void onConnect(String serial) {
                setStatus(StatusConstants.CONNECTED);
                progress.setVisibility(View.VISIBLE);
                Log.d(LOG_TAG, "onConnect: serial" + serial);

                Intent intent = new Intent(me, SensorService.class);
                intent.setAction(ACTION_CONNECT);
                intent.putExtra(MOVESENSE_SERIAL, serial);
                startService(intent);
            }

            @Override
            public void onConnectionComplete(String macAddress, String serial) {
                Log.d(LOG_TAG, "onConnectionComplete macAddress: " + serial + ", serial: " + serial);
                progress.setVisibility(View.INVISIBLE);

                for (MyScanResult sr : mScanResArrayList) {
                    if (sr.macAddress.equalsIgnoreCase(macAddress)) {
                        sr.markConnected(serial);
                        break;
                    }
                }
                mScanResArrayAdapter.notifyDataSetChanged();

                Intent intent = new Intent(me, SensorService.class);
                intent.setAction(ACTION_TRACK);
                intent.putExtra(MOVESENSE_SERIAL, serial);
                startService(intent);
            }

            @Override
            public void onError(MdsException e) {
                Log.e(LOG_TAG, "onError:" + e);
                progress.setVisibility(View.INVISIBLE);

                showConnectionError(e);
            }

            @Override
            public void onDisconnect(String bleAddress) {
                Log.d(LOG_TAG, "onDisconnect: " + bleAddress);
                progress.setVisibility(View.INVISIBLE);

                for (MyScanResult sr : mScanResArrayList) {
                    if (bleAddress != null && bleAddress.equals(sr.macAddress)) {
                        // Unsubscribe all from possible
//                        if (sr.connectedSerial != null &&
//                                DataLoggerActivity.s_INSTANCE != null &&
//                                sr.connectedSerial.equals(DataLoggerActivity.s_INSTANCE.connectedSerial)) {
//                            DataLoggerActivity.s_INSTANCE.finish();
//                        }
                        sr.markDisconnected();
                    }
                }

                mScanResArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void showConnectionError(MdsException e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Connection Error:")
                .setMessage(e.getMessage());
        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        stopScan();
        for (MyScanResult sr : mScanResArrayList) {
            mMds.disconnect(sr.macAddress);
        }

        super.onDestroy();
    }

    private void setStatus(String text) {
        statusText.setText(text);
    }
}
