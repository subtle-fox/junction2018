package com.petersfortress.movesense.junction2018;

import com.petersfortress.movesense.junction2018.models.Point;

public interface DataFlowInteractor {
    public void onUpdated(final Point point);
}
