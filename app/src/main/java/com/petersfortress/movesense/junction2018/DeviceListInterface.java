package com.petersfortress.movesense.junction2018;

public interface DeviceListInterface {
    void onClick(int position);
    void onLongClick(int position);
}
